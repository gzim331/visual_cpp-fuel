#pragma once

namespace zadanie2 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::IO;

	/// <summary>
	/// Summary for MyForm1
	/// </summary>
	public ref class MyForm1 : public System::Windows::Forms::Form
	{
	public:
		MyForm1(void)
		{
			InitializeComponent();
			//StreamReader ^ wczytuj = gcnew StreamReader("historia.txt");
			//String ^ zawartosc;
			//while (zawartosc = wczytuj->ReadLine())
			//	textBox1->Text = zawartosc;
			String ^ fileName = "historia.txt",
				^ buffor = "",
				^ readDate = "";

			StreamReader ^ sr = File::OpenText(fileName);
			try
			{
				while (buffor = sr->ReadLine())
				{
					readDate += buffor + "\r\n";
				}
			}
			finally
			{
				textBox1->Text = readDate;
				sr->Close();
			}
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MyForm1()
		{
			if (components)
			{
				delete components;
			}
		}

	protected:
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::TextBox^  textBox1;




	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->SuspendLayout();
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->label1->Location = System::Drawing::Point(121, 9);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(85, 25);
			this->label1->TabIndex = 2;
			this->label1->Text = L"Historia";
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(27, 51);
			this->textBox1->Name = L"textBox1";
			this->textBox1->Multiline = true;
			this->textBox1->ScrollBars = ScrollBars::Vertical;
			this->textBox1->Size = System::Drawing::Size(274, 300);
			this->textBox1->TabIndex = 3;
			this->textBox1->Text = L"Welcome!";
			// 
			// MyForm1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(329, 368);
			this->Controls->Add(this->textBox1);
			this->Controls->Add(this->label1);
			this->Name = L"MyForm1";
			this->Text = L"Historia";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void panel1_Paint(System::Object^  sender, System::Windows::Forms::PaintEventArgs^  e) {

	}
};
}
