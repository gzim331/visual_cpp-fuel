#pragma once
#include "MyForm1.h"
namespace zadanie2 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::IO;

	/// <summary>
	/// Summary for MyForm
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form
	{
	public:
		MyForm(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::TextBox^  trasaTextBox;
	private: System::Windows::Forms::TextBox^  spalanieTextBox;
	private: System::Windows::Forms::TextBox^  cenaTextBox;
	protected:

	protected:

	protected:



	private: System::Windows::Forms::Button^  licz;

	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::NumericUpDown^  osobyNumericUpDown;


	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::TextBox^  wagaTextBox;

	private: System::Windows::Forms::Label^  result;
	private: System::Windows::Forms::Label^  label1;

	private: System::Windows::Forms::ContextMenuStrip^  contextMenuStrip1;
	private: System::Windows::Forms::ToolStripMenuItem^  historiaToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  zamknijToolStripMenuItem;
	private: System::Windows::Forms::MenuStrip^  menuStrip1;
	private: System::Windows::Forms::ToolStripMenuItem^  plikToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  historiaToolStripMenuItem1;
	private: System::Windows::Forms::ToolStripMenuItem^  zamknijToolStripMenuItem1;
	private: System::Windows::Forms::ToolStripMenuItem^  oProgramieToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  oProgramieToolStripMenuItem1;
	private: System::Windows::Forms::ComboBox^  comboBox1;
	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::PictureBox^  pictureBox1;


	private: System::ComponentModel::IContainer^  components;



	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(MyForm::typeid));
			this->trasaTextBox = (gcnew System::Windows::Forms::TextBox());
			this->spalanieTextBox = (gcnew System::Windows::Forms::TextBox());
			this->cenaTextBox = (gcnew System::Windows::Forms::TextBox());
			this->licz = (gcnew System::Windows::Forms::Button());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->osobyNumericUpDown = (gcnew System::Windows::Forms::NumericUpDown());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->wagaTextBox = (gcnew System::Windows::Forms::TextBox());
			this->result = (gcnew System::Windows::Forms::Label());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->contextMenuStrip1 = (gcnew System::Windows::Forms::ContextMenuStrip(this->components));
			this->historiaToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->zamknijToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->plikToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->historiaToolStripMenuItem1 = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->zamknijToolStripMenuItem1 = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->oProgramieToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->oProgramieToolStripMenuItem1 = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->comboBox1 = (gcnew System::Windows::Forms::ComboBox());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->osobyNumericUpDown))->BeginInit();
			this->contextMenuStrip1->SuspendLayout();
			this->menuStrip1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->BeginInit();
			this->SuspendLayout();
			// 
			// trasaTextBox
			// 
			this->trasaTextBox->Location = System::Drawing::Point(449, 104);
			this->trasaTextBox->Name = L"trasaTextBox";
			this->trasaTextBox->Size = System::Drawing::Size(155, 20);
			this->trasaTextBox->TabIndex = 1;
			this->trasaTextBox->Text = L"Ca�kowita trasa (km)";
			// 
			// spalanieTextBox
			// 
			this->spalanieTextBox->Location = System::Drawing::Point(449, 157);
			this->spalanieTextBox->Name = L"spalanieTextBox";
			this->spalanieTextBox->Size = System::Drawing::Size(155, 20);
			this->spalanieTextBox->TabIndex = 2;
			this->spalanieTextBox->Text = L"Spalanie/100 (L)";
			// 
			// cenaTextBox
			// 
			this->cenaTextBox->Location = System::Drawing::Point(449, 208);
			this->cenaTextBox->Name = L"cenaTextBox";
			this->cenaTextBox->Size = System::Drawing::Size(155, 20);
			this->cenaTextBox->TabIndex = 3;
			this->cenaTextBox->Text = L"Cena (za litr)";
			// 
			// licz
			// 
			this->licz->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->licz->Location = System::Drawing::Point(626, 104);
			this->licz->Name = L"licz";
			this->licz->Size = System::Drawing::Size(159, 54);
			this->licz->TabIndex = 4;
			this->licz->Text = L"Przelicz";
			this->licz->UseVisualStyleBackColor = true;
			this->licz->Click += gcnew System::EventHandler(this, &MyForm::button1_Click);
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 14.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->label2->Location = System::Drawing::Point(492, 271);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(95, 24);
			this->label2->TabIndex = 5;
			this->label2->Text = L"Ilo�� os�b";
			// 
			// osobyNumericUpDown
			// 
			this->osobyNumericUpDown->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 14.25F, System::Drawing::FontStyle::Regular,
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(238)));
			this->osobyNumericUpDown->Location = System::Drawing::Point(593, 269);
			this->osobyNumericUpDown->Name = L"osobyNumericUpDown";
			this->osobyNumericUpDown->Size = System::Drawing::Size(42, 29);
			this->osobyNumericUpDown->TabIndex = 6;
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 14.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->label3->Location = System::Drawing::Point(641, 271);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(32, 24);
			this->label3->TabIndex = 7;
			this->label3->Text = L"po";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 14.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->label4->Location = System::Drawing::Point(726, 271);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(30, 24);
			this->label4->TabIndex = 8;
			this->label4->Text = L"kg";
			// 
			// wagaTextBox
			// 
			this->wagaTextBox->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 14.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->wagaTextBox->Location = System::Drawing::Point(679, 269);
			this->wagaTextBox->Name = L"wagaTextBox";
			this->wagaTextBox->Size = System::Drawing::Size(41, 29);
			this->wagaTextBox->TabIndex = 9;
			this->wagaTextBox->Text = L"0";
			this->wagaTextBox->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			// 
			// result
			// 
			this->result->AutoSize = true;
			this->result->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 26.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->result->ForeColor = System::Drawing::Color::MidnightBlue;
			this->result->Location = System::Drawing::Point(576, 331);
			this->result->Name = L"result";
			this->result->Size = System::Drawing::Size(0, 39);
			this->result->TabIndex = 10;
			this->result->TextAlign = System::Drawing::ContentAlignment::BottomCenter;
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 27.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->label1->ImageAlign = System::Drawing::ContentAlignment::TopCenter;
			this->label1->Location = System::Drawing::Point(308, 36);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(296, 42);
			this->label1->TabIndex = 11;
			this->label1->Text = L"Przelicznik Drogi";
			this->label1->TextAlign = System::Drawing::ContentAlignment::TopCenter;
			this->label1->Click += gcnew System::EventHandler(this, &MyForm::label1_Click);
			// 
			// contextMenuStrip1
			// 
			this->contextMenuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {
				this->historiaToolStripMenuItem,
					this->zamknijToolStripMenuItem
			});
			this->contextMenuStrip1->Name = L"contextMenuStrip1";
			this->contextMenuStrip1->Size = System::Drawing::Size(118, 48);
			this->contextMenuStrip1->Text = L"Plik";
			this->contextMenuStrip1->Opening += gcnew System::ComponentModel::CancelEventHandler(this, &MyForm::contextMenuStrip1_Opening);
			// 
			// historiaToolStripMenuItem
			// 
			this->historiaToolStripMenuItem->Name = L"historiaToolStripMenuItem";
			this->historiaToolStripMenuItem->Size = System::Drawing::Size(117, 22);
			this->historiaToolStripMenuItem->Text = L"Historia";
			// 
			// zamknijToolStripMenuItem
			// 
			this->zamknijToolStripMenuItem->Name = L"zamknijToolStripMenuItem";
			this->zamknijToolStripMenuItem->Size = System::Drawing::Size(117, 22);
			this->zamknijToolStripMenuItem->Text = L"Zamknij";
			this->zamknijToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::zamknijToolStripMenuItem_Click);
			// 
			// menuStrip1
			// 
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {
				this->plikToolStripMenuItem,
					this->oProgramieToolStripMenuItem
			});
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Size = System::Drawing::Size(884, 24);
			this->menuStrip1->TabIndex = 13;
			this->menuStrip1->Text = L"menuStrip1";
			// 
			// plikToolStripMenuItem
			// 
			this->plikToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {
				this->historiaToolStripMenuItem1,
					this->zamknijToolStripMenuItem1
			});
			this->plikToolStripMenuItem->Name = L"plikToolStripMenuItem";
			this->plikToolStripMenuItem->Size = System::Drawing::Size(38, 20);
			this->plikToolStripMenuItem->Text = L"Plik";
			// 
			// historiaToolStripMenuItem1
			// 
			this->historiaToolStripMenuItem1->Name = L"historiaToolStripMenuItem1";
			this->historiaToolStripMenuItem1->Size = System::Drawing::Size(117, 22);
			this->historiaToolStripMenuItem1->Text = L"Historia";
			this->historiaToolStripMenuItem1->Click += gcnew System::EventHandler(this, &MyForm::historiaToolStripMenuItem1_Click);
			// 
			// zamknijToolStripMenuItem1
			// 
			this->zamknijToolStripMenuItem1->Name = L"zamknijToolStripMenuItem1";
			this->zamknijToolStripMenuItem1->Size = System::Drawing::Size(117, 22);
			this->zamknijToolStripMenuItem1->Text = L"Zamknij";
			this->zamknijToolStripMenuItem1->Click += gcnew System::EventHandler(this, &MyForm::zamknijToolStripMenuItem1_Click);
			// 
			// oProgramieToolStripMenuItem
			// 
			this->oProgramieToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->oProgramieToolStripMenuItem1 });
			this->oProgramieToolStripMenuItem->Name = L"oProgramieToolStripMenuItem";
			this->oProgramieToolStripMenuItem->Size = System::Drawing::Size(40, 20);
			this->oProgramieToolStripMenuItem->Text = L"Info";
			this->oProgramieToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::oProgramieToolStripMenuItem_Click);
			// 
			// oProgramieToolStripMenuItem1
			// 
			this->oProgramieToolStripMenuItem1->Name = L"oProgramieToolStripMenuItem1";
			this->oProgramieToolStripMenuItem1->Size = System::Drawing::Size(141, 22);
			this->oProgramieToolStripMenuItem1->Text = L"O programie";
			this->oProgramieToolStripMenuItem1->Click += gcnew System::EventHandler(this, &MyForm::oProgramieToolStripMenuItem1_Click);
			// 
			// comboBox1
			// 
			this->comboBox1->AutoCompleteCustomSource->AddRange(gcnew cli::array< System::String^  >(7) {
				L"Mustang GT", L"Mustang EcoBoost",
					L"Porsche 964", L"Cadillac DeVille \'61", L"Aston Martin Vantage", L"Buick Riviera \'63", L"Multipla"
			});
			this->comboBox1->FormattingEnabled = true;
			this->comboBox1->Items->AddRange(gcnew cli::array< System::Object^  >(7) {
				L"Mustang GT", L"Mustang EcoBoost", L"Porsche 964",
					L"Cadillac DeVille \'61", L"Aston Martin Vantage", L"Buick Riviera \'63", L"Multipla"
			});
			this->comboBox1->Location = System::Drawing::Point(626, 207);
			this->comboBox1->Name = L"comboBox1";
			this->comboBox1->Size = System::Drawing::Size(143, 21);
			this->comboBox1->TabIndex = 14;
			this->comboBox1->Text = L"Samoch�d";
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(623, 182);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(153, 13);
			this->label5->TabIndex = 15;
			this->label5->Text = L"Domy�lne spalanie samochodu";
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(776, 200);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(44, 35);
			this->button1->TabIndex = 16;
			this->button1->Text = L"OK";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &MyForm::button1_Click_1);
			// 
			// pictureBox1
			// 
			this->pictureBox1->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->pictureBox1->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox1.Image")));
			this->pictureBox1->Location = System::Drawing::Point(52, 104);
			this->pictureBox1->Name = L"pictureBox1";
			this->pictureBox1->Size = System::Drawing::Size(343, 266);
			this->pictureBox1->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pictureBox1->TabIndex = 17;
			this->pictureBox1->TabStop = false;
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(884, 412);
			this->Controls->Add(this->pictureBox1);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->label5);
			this->Controls->Add(this->comboBox1);
			this->Controls->Add(this->menuStrip1);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->result);
			this->Controls->Add(this->wagaTextBox);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->osobyNumericUpDown);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->licz);
			this->Controls->Add(this->cenaTextBox);
			this->Controls->Add(this->spalanieTextBox);
			this->Controls->Add(this->trasaTextBox);
			this->MainMenuStrip = this->menuStrip1;
			this->Name = L"MyForm";
			this->Text = L"Fuel";
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->osobyNumericUpDown))->EndInit();
			this->contextMenuStrip1->ResumeLayout(false);
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
		private: System::Double liczenie(double trasa, double spalanie, double cena, double ile_osob, double waga_osob) { //oddzielna funkcja licz�ca
			System::Double ciezar = ile_osob*waga_osob;  //ile osob PO mniej wiecej ile kg.
			System::Double ciezar_w = (0.6*ciezar) / 100; //jak ci�ar dodatkowych os�b wp�ywa na spalanie
			System::Double wynik = (((spalanie + ciezar_w)*trasa) / 100)*cena; // scalam wszystko i wychodzi wynik
			return (int)(wynik * 100) / 100.00;   //zwracam wynik w postaci DO dw�ch liczb po przecinku
		}

	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
		System::Double trasa = (Double::Parse(trasaTextBox->Text));			///////
		System::Double spalanie = (Double::Parse(spalanieTextBox->Text));	///////
		System::Double cena = (Double::Parse(cenaTextBox->Text));			// przypisuje kontrolki do zmiennych i parsuje na odpowiednie typy

		System::Int32 ile_osob = (Int32::Parse(osobyNumericUpDown->Text));	//////
		System::Int32 waga_osob = (Int32::Parse(wagaTextBox->Text));		//////

		result->Text = liczenie(trasa, spalanie, cena, ile_osob, waga_osob).ToString()+" PLN"; // do result(label) przekazuje wynik, konwertuje na stringa i dodaje ci�g znak�w(pln)
		
		TextWriter ^zapisz = File::AppendText(L"historia.txt");
		zapisz->WriteLine("trasa: "+trasa.ToString());
		zapisz->WriteLine("spalanie: "+spalanie.ToString());
		zapisz->WriteLine("cena: " + trasa.ToString());
		zapisz->WriteLine("dodatkowe_osoby: "+ile_osob.ToString()+" po "+waga_osob.ToString()+" kg");
		zapisz->WriteLine("WYNIK = "+result->Text+"\n");
		zapisz->Close();

		label1->Text = "Congratulation";
		pictureBox1->Load("img/result.gif");
	}
private: System::Void zamknijToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
}
private: System::Void contextMenuStrip1_Opening(System::Object^  sender, System::ComponentModel::CancelEventArgs^  e) {
}
private: System::Void oProgramieToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
}
private: System::Void oProgramieToolStripMenuItem1_Click(System::Object^  sender, System::EventArgs^  e) {
	MessageBox::Show(",,Fuel - Przelicznik drogi''\nZadanie kontrolne nr 3\nCopyright (R) 2018 by Micha� Zimka", "Fuel");
}
private: System::Void zamknijToolStripMenuItem1_Click(System::Object^  sender, System::EventArgs^  e) {
	Close();
}
private: System::Void historiaToolStripMenuItem1_Click(System::Object^  sender, System::EventArgs^  e) {
	MyForm1^ newForm = gcnew MyForm1;
	newForm->ShowDialog();
}
private: System::Void label1_Click(System::Object^  sender, System::EventArgs^  e) {
}
private: System::Void button1_Click_1(System::Object^  sender, System::EventArgs^  e) {
	Object^ item = comboBox1->SelectedItem;
	
	switch (comboBox1->SelectedIndex) {
	case 0:
		spalanieTextBox->Text = "15";
		label1->Text = "Mustang GT";
		pictureBox1->Load("img/mustangGT.jpg");
		break;
	case 1:
		spalanieTextBox->Text = "9";
		label1->Text = "Mustang EcoBoost";
		pictureBox1->Load("img/mustangEco.jpg");
		break;
	case 2:
		spalanieTextBox->Text = "18";
		label1->Text = "Porsche 964";
		pictureBox1->Load("img/porsche964.jpg");
		break;
	case 3:
		spalanieTextBox->Text = "22.5";
		label1->Text = "Cadillac DeVille '61";
		pictureBox1->Load("img/cadillac.jpg");
		break;
	case 4:
		spalanieTextBox->Text = "16.4";
		label1->Text = "Aston Martin Vantage";
		pictureBox1->Load("img/aston.jpg");
		break;
	case 5:
		spalanieTextBox->Text = "17.8";
		label1->Text = "Buick Riviera '63";
		pictureBox1->Load("img/buick.jpg");
		break;
	case 6:
		spalanieTextBox->Text = "6.0";
		label1->Text = "Multipla";
		pictureBox1->Load("img/multipla.jpg");
		break;
	default:
		MessageBox::Show("Nie ma takiej opcji");
		break;
	}
}
};
}
